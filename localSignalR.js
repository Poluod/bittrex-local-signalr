var express = require('express');
var SignalRJS = require('signalrjs');

//Init SignalRJs
var signalR = SignalRJS();

signalR.hub('publichub',{
    getMarkets : function(hubName){
        console.log('--------publicHub------------------getMarkets-------------');
        this.clients.all.invoke('updateMarket').withArgs(['getMarkets'])
    },
    getTicks : function (hubName) {
        console.log('--------publicHub------------------getTicks-------------');
        this.clients.all.invoke('updateTicks').withArgs(['getTicks'])
    },
    getBook : function (hubName) {
        console.log('--------publicHub------------------getBook-------------');
        this.clients.all.invoke('broadcast').withArgs(['getBook'])
    },
    subscribeMarket : function (hubName) {
        console.log('--------publicHub------------------subscribeMarket-------------');
        this.clients.all.invoke('updateMarket').withArgs(['subscribeMarket'])
    },
    subscribeMarkets : function (hubName) {
        console.log('--------publicHub------------------subscribeMarkets-------------');
        this.clients.all.invoke('updateMarket').withArgs(['subscribeMarkets'])
    },
    subscribeLots : function (hubName) {
        console.log('--------publicHub------------------subscribeLots-------------');
        this.clients.all.invoke('updateLot').withArgs(['subscribeLots'])
    },
    unSubscribeMarket : function (hubName) {
        console.log('--------publicHub------------------unSubscribeMarket-------------');
        this.clients.all.invoke('broadcast').withArgs(['unSubscribeMarket'])
    },
    unSubscribeMarkets : function (hubName) {
        console.log('--------publicHub------------------unSubscribeMarkets-------------');
        this.clients.all.invoke('broadcast').withArgs(['unSubscribeMarkets'])
    },
    unsubscribeLots : function (hubName) {
        console.log('--------publicHub------------------unsubscribeLots-------------');
        this.clients.all.invoke('broadcast').withArgs(['unsubscribeLots'])
    },
    getCommissionRates : function (hubName) {
        console.log('--------publicHub------------------getCommissionRates-------------');
        this.clients.all.invoke('broadcast').withArgs(['getCommissionRates'])
    }
});

// signalR.hub('privatehub',{
//     getMarkets : function(hubName){
//         this.clients.all.invoke('broadcast').withArgs(['getMarkets'])
//     },
//     getFavoriteMarkets : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getFavoriteMarkets'])
//     },
//     getOrdersCount : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getOrdersCount'])
//     },
//     getOrders : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getOrders'])
//     },
//     getOpenOrders : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getOpenOrders'])
//     },
//     getOrdersHistory : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getOrdersHistory'])
//     },
//     getCompletedOrders : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getCompletedOrders'])
//     },
//     getBalances : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getBalances'])
//     },
//     getAddresses : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getAddresses'])
//     },
//     requestDepositAddress : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['requestDepositAddress'])
//     },
//     getDepositsHistory : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getDepositsHistory'])
//     },
//     getWithdrawalsHistory : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getWithdrawalsHistory'])
//     },
//     withdraw : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['withdraw'])
//     },
//     favoriteMarket : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['favoriteMarket'])
//     },
//     unfavoriteMarket : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['unfavoriteMarket'])
//     },
//     getCurrentCommissionRate : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getCurrentCommissionRate'])
//     },
//     buy : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['buy'])
//     },
//     sell : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['sell'])
//     },
//     cancel : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['cancel'])
//     },
//     cancelWithdrawal : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['cancelWithdrawal'])
//     },
//     GetApiKeys : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['GetApiKeys'])
//     },
//     UpdateApiKey : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['UpdateApiKey'])
//     },
//     EnableApiKey : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['EnableApiKey'])
//     },
//     DisableApiKey : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['DisableApiKey'])
//     },
//     DeleteApiKey : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['DeleteApiKey'])
//     },
//     CreateApiKey : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['CreateApiKey'])
//     },
//     getUserInfo : function (hubName) {
//         this.clients.all.invoke('broadcast').withArgs(['getUserInfo'])
//     }
// });

var server = express();
server.use( function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});
server.use(express.static(__dirname));
server.use(signalR.createListener());
server.listen(3000);