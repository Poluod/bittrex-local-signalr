/**
 * Created by MUZH on 2017/9/13.
 */

'use strict';
const _ = require('lodash'),
  fs = require('fs'),
  defaultAssets = require('./config/assets/default'),
  gulp = require('gulp'),
  gulpLoadPlugins = require('gulp-load-plugins'),
  runSequence = require('run-sequence'),
  plugins = gulpLoadPlugins(),
  path = require('path');

// sass to css
gulp.task('sass', () => {
  return gulp.src(defaultAssets.client.sass)
    .pipe(plugins.sass())
    .pipe(plugins.autoprefixer())
    .pipe(gulp.dest('modules'));
});

// nodemon task
gulp.task('nodemon', () => {
  return plugins.nodemon({
    script: 'server.js',
    ext: 'js,html',
    verbose: true,
    tasks: ['eslint'],
    watch: _.union(defaultAssets.server.views, defaultAssets.server.allJS, defaultAssets.server.config)
  })
    .on('config:update', () => {
      let config = require('./config/config');
      // Delay before server listens on port
      setTimeout(() => {
        require('open')('http://localhost:' + config.port);
      }, 3000);
    })
    .on('restart', () => {
      // Delay before server listens on port
      setTimeout(() => {
        require('fs').writeFileSync('.rebooted', 'rebooted');
      }, 2000);
    });
});

// watch task
gulp.task('watch', () => {
  // Start livereload
  plugins.refresh.listen();
// Add watch rules
  gulp.watch(defaultAssets.client.js, ['eslint']).on('change', plugins.refresh.changed);
  gulp.watch(defaultAssets.client.sass, ['sass', 'csslint']).on('change', plugins.refresh.changed);
  gulp.watch(defaultAssets.client.views).on('change', plugins.refresh.changed);
});

// Run the project in development mode
gulp.task('default', (done) => {
  runSequence('env:dev', 'lint', ['nodemon', 'watch'], done);
});
